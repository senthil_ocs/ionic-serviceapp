angular.module('starter.controllers', [])

.controller('MapCtrl', function($scope, $ionicLoading,$ionicSideMenuDelegate) {
  // $scope.hiddenMenu = false;

  // $scope.toggleLeft = function() {
  //           $ionicSideMenuDelegate.toggleLeft();
  //       };

  // $scope.$watch(
  //   function() {
  //     return $ionicSideMenuDelegate.isOpen();
  //   },
  //   function(value) {
  //     $scope.hiddenMenu = !value;
  //     $scope.$apply();
  // });

  $scope.mapCreated = function(map) {
    $scope.map = map;
  };
  $scope.toggleSideMenu = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.centerOnMe = function () {
    console.log("Centering");
    if (!$scope.map) {
      return;
    }

    $scope.loading = $ionicLoading.show({
      content: 'Getting current location...',
      showBackdrop: false
    });

    navigator.geolocation.getCurrentPosition(function (pos) {
      console.log('Got pos', pos);
      $scope.map.setCenter(new google.maps.LatLng(pos.coords.latitude, pos.coords.longitude));
      $scope.loading.hide();

      var directionsDisplay = new google.maps.DirectionsRenderer();
      var directionsService = new google.maps.DirectionsService();


      console.log($scope.map);
      directionsDisplay.setMap($scope.map);
         $scope.directions = {
    origin: "Collins St, Melbourne, Australia",
    destination: "MCG Melbourne, Australia",
    showList: false
  }
    
function calcRoute() {
        var request = {
          origin: $scope.directions.origin,
      destination: $scope.directions.destination,
      travelMode: google.maps.DirectionsTravelMode.DRIVING
        };

        directionsService.route(request, function(response, status) {
          if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);   
            directionsDisplay.setMap(map);           
            console.log('enter!');  
          }
        });


      }
      calcRoute();
    }, function (error) {
      alert('Unable to get location: ' + error.message);
    });
  };
});
